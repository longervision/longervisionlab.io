---
layout: page
title: About
permalink: /about/
---

Hi, this is **NOBODY** having been studying and researching in computer vision. I have to do things **ALL** by myself, **ALL** from scratch. **Tough**, isn’t it?

At the very beginning, I've got **NO** idea what is this static site for? I was just about to get myself familiar with how to host on [Gitlab]. But, the site has to have some motive. Even now, I've **NO** idea what to write here.

:sob::sob::sob::sob::sob::sob::sob::sob::sob:

Finally, I came up with an interesting but extremely tough topic. I even have **NO** idea if I myself am able to understand what I'm talking about. 

In this blog site, we're going to cover the following vision topics, including:

- [Stereo](https://en.wikipedia.org/wiki/Computer_stereo_vision)
- [Manifold](https://en.wikipedia.org/wiki/Manifold)
- [Panorama](https://en.wikipedia.org/wiki/Panorama)
- [Spherical CNNs](https://openreview.net/pdf?id=Hkbd5xZRb)
- etc.

Hopefully, I can make myself understood in the end.

:pray::pray::pray::pray::pray::pray::pray::pray::pray:

[Gitlab]:   https://gitlab.com/
